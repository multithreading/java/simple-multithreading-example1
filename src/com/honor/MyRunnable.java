package com.honor;

/*
 * Another way to create a thread. Instead of creating an instance of Thread class 
 * we can implement Runnable interface and provide an instance of this class to the Thread constructor. 
 * See the main method.
 * */
public class MyRunnable implements Runnable {

	@Override
	public void run() {
		System.out.println("Hello from MyRunnable's run method.");
	}

}
