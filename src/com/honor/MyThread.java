package com.honor;

public class MyThread extends Thread {
/*
 * We have to implement the run method but we don't call it directly. JVM will call it for us. 
 * We call the start method.
 * If we call the run method directly, instead of creating a new thread, 
 * the run method will be run on the same thread that calls the run method!
 * */
	@Override
	public void run() {
		System.out.println(TColors.ANSI_RED + "Hello from --" + currentThread().getName() + "--");
		
		try {
			System.out.println(TColors.ANSI_RED + "--" + currentThread().getName() + "--" + " going to sleep");
			Thread.sleep(4000);
		}
		/*
		 * catching InterruptedException is one of the ways for a thread to notice
		 * it is being interrupted. Another way is to call the interrupted method periodically.
		 * */
		catch(InterruptedException ex){
			System.out.println(TColors.ANSI_RED + "--" + currentThread().getName() + "--" + " was waken up");
			return;
		}
		System.out.println(TColors.ANSI_RED + "--" + currentThread().getName() + "--" + " 4 seconds. I am awake.");
	}
	
}
