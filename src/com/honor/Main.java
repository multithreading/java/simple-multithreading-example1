package com.honor;

public class Main {

	/* Most of the my system generated the output below. Your system could generate different results.
	 * Hello from Main thread. 
	 * Hello from MyThread. 
	 * Hello again from Main thread. 
	 * Hello from Anonymous Thread.
	 * 
	 * joining threads:
	 * When we join a thread to a 2nd thread, 1st thread will wait for the 2nd thread to terminate and 
	 * then it will continue to execute. 
	 * We call the join method of the thread that we want the current thread to join to.
	 * 
	 * It is also possible to set priorities for threads but it still does not guarantee the execution order of the threads.
	 * Decision is still with the operating system. You can think of setting priority as a suggestion mechanism.
	 * Not all OSs support setting priority, some will neglect the priority specification.
	 */
	public static void main(String[] args) {
		System.out.println(TColors.ANSI_GREEN + "Hello from Main thread.");
		MyThread myThread = new MyThread();
		myThread.setName("MyThread");
		myThread.start();
		//myThread.run(); -> this would print Hello from --main-- instead of --MyThread-- see MyThread class's comments.
		
		new Thread() {
			public void run() {
				System.out.println(TColors.ANSI_BLUE + "Hello from Anonymous Thread.");
			}
		}.start();

		Thread myRunnableThread = new Thread(new MyRunnable());
		myRunnableThread.start();
		
		//using an anonymous class of MyRunnable
		Thread myAnonymousRunnableThread = new Thread(new MyRunnable() {
			public void run() {
				System.out.println("Hello from myAnonymousRunnableThread");
				try {
					System.out.println("'myAnonymousRunnableThread' joining 'myThread'");
					/*
					 * We can provide a timeout parameter to the join method. 
					 * In the joined thread is not finished in the specified period of time, then 
					 * our thread will resume execution anyway.
					 * */
					myThread.join(2000);
					System.out.println("'myThread terminated or timed out. "
							+ "myAnonymousRunnableThread started again.'");
				}catch(InterruptedException ex) {
					System.out.println("myAnonymousRunnableThread was waken up by another thread.");
				}
			}
		});
		myAnonymousRunnableThread.start();
		
		//interrupting myThread below
		//myThread.interrupt();
		
		System.out.println(TColors.ANSI_GREEN + "Hello again from Main thread.");
	}

}
